import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }

  setEmojis(emojis) {
    this.emojis = emojis;

    // Create the p element and attach it's text Node:
    let para = document.createElement("p");
    let paraContent = document.createTextNode(emojis);
    para.appendChild(paraContent);

    // Select the div #emojis and inject the p into the div:
    let wrapper = document.querySelector("div#emojis");
    wrapper.textContent = "";
    wrapper.appendChild(para);
  }

  addBananas() {
    let monkeysWithBananas = this.emojis.map(monkey => {
      return monkey + this.banana;
    });

    this.setEmojis(monkeysWithBananas);
  }
}
